# Stupidly Determined

My eldest son and I were running in the Sydney City-to-Surf, a brutally hilly 14km fun run. He was almost 5 years old at the time, but already showing an uncanny ability to push his physical limits and was determined to run the whole way without stopping or walking. At around the 12km mark, it was clear he was in a great deal of pain and I tried to find ways to distract him, so I drew his attention to some of the people around us and we speculated on how they might be feeling.

One was wearing a latex banana costume and was clearly wilting in the heat. My son announced that he wanted "to beat the banana!". It is difficult to fully describe what a father feels when their son displays a level of grit that is rare even amongst adults [^3]. He did indeed beat the banana.

But it was a different participant who inspired *me*. Just a little ahead of us was a somewhat overweight woman in her late 30s, with a sturdy, compact, wombat-like build, displaying a singularly inelegant, plodding running style. Red-faced, panting and throwing herself forward one-step at a time through sheer force of will. 

What on earth was keeping her going? She was struggling horribly. She had nothing to gain - there are no prizes for adults back in the pack; the people who are impressed by 4 year-old running 14km won't be moved by a 38-year old doing the same. After more than an hour and a half, a couple of minutes here or there didn't seem to matter enough to account for what she was putting herself through.

The answer, though, was written neatly and succinctly in pink neon lettering on the back of her over-tight black T-Shirt. It said simply ... "Stupidly Determined".

As if it had been a Zen Koan, these two words penetrated my perception of reality, and I was enlightened. I realised how often in my life I've faced variations of her situation: no hope left of achieving any meaningful success, but still having to complete the task, yet unable to summon the motivation to keep going. Of course! The answer was Stupidly Determined.

The Reckless Courage of the Noble Warrior
-----------------------------------------

There is a parallel to be drawn from the great heroic stories of noble knights and warriors facing death with honour and duty.

In Bushido, the Way of the Samurai, courageousness and willingness to die in the service of one's retainer, even when the potential gain is minimal, is a virtue in its own right [^1].

Similarly, in Tennison's poem "The Charge of the Light Brigade", set during the Crimean War at the zenith of the British Empire, the sword-wielding British horsemen are mistakenly commanded to undertake a frontal assault on a heavily fortified Russian artillary post. Understanding the futility of the charge they nevertheless ride boldly toward the enemy lines:

> Not though the soldier knew  
> Someone had blundered.  
> Theirs not to make reply,  
> Theirs not to reason why,  
> Theirs but to do and die.  
> Into the valley of Death  
> Rode the six hundred.  
> ...  
> Honour the charge they made!  
> Honour the Light Brigade,  
> Noble six hundred!  

These expressions of reckless courage in the face of futility are the very essence of heroism. For a true hero, the nobility of the act is its own reward.

Something seems faintly stupid and absurd about these displays as viewed through modern eyes, though. It's easy to scoff cynically, regarding as foolish anyone who could be manipulated by cowardly, inept bureaucrats with an insufficient regard for human life, into accepting a pointless death.

But "reckless courage" is clearly a powerful motivator. Powerful enough to inspire acts of great bravery and sacrifice. Powerful enough to overcome the great human urge for self-preservation. Powerful enough to overpower the debilitation provoked by mortal fear. Powerful enough to inspire ordinary young men to fight bravely for their country in a time of war.

If you were able to summon the mindset of a noble warrior at will, surely it could get you through any trial you were facing.

But for our noble fun-running heroine, what reward could she possibly expect that would justify damaging her body and suffering intense pain? Surely not the tacky finisher's medallion awarded to every one of the 80,000+ fanny-packed middle-aged power-walkers and pallid-skinned, atrophic, desk-bound troglodytes?

The Psychology of High Performers
---------------------------------

Motivating ourselves to do what we know we should do is hard. It is hard to lose weight. It is hard to get fit. It is hard to eat healthily and in moderation. It is hard to refrain from one more drink or one more cigarette. It is hard to stop yourself procrastinating. And it is hard to be a kinder, more patient, and more honest person. 

Everyday we fight a losing battle against our deepest instincts, striving to be more than we are. We are racked by self-doubt, deflated by negative feedback, powerless in the face of our physical urges and enveloped by a suffocating fog of futility and self-hate. Small setbacks, constant resistance and persistent failure leave us weakened and exhausted, vulnerable to temptation and relapse.

One weapon offered by psychologists for turning the tide of battle in your favour is "cognitive reappraisal", or more colloquially "the power of positive thinking". This attempts to replace "negative self-talk" with more productive lines of thinking. The goal is to be able to recognise and acknowledge your negative thoughts dispassionately, and replace them with more positive and productive messages [^2].

In Tim Gallwey's excellent book, "The Inner Game of Tennis" [^4], he explains that *any* conscious judgement, whether positive *or* negative, is inherently damaging to performance. Instead, his approach aims to deflect conscious attention away from judgemental or controlling thoughts, redirecting it to salient environmental details, such as the spin on the seam of the ball. In this way, your body can get on with performing its task undisturbed.

In both cases, the key is to redirect your conscious mind away from activities that are not directly related to the current moment; thoughts such as: fear of making mistakes, concern for the outcome, or the consequences of failure. And instead focus on the immediate needs of the situation. To be "in the moment" as the self-help books put it.

In every way, this is the opposite of the kind of intelligence we spend most of our lives cultivating: "seeing the big picture", "planning for the future", "extrapolating intentional causes", "reading hidden motives". To be a good athlete can mean learning a special kind of productive stupidity.

The Virtue of Being Stupidly Determined
---------------------------------------

To me, Stupidly Determined is a delightfully succinct expression that can help trigger this mindset. Stupidly Determined reminds you not to worry about outcomes, failure or the future. Stupidly Determined focuses your attention on one thing you can do right now: accept your pain and wear it with pride. Stupidly Determined turns futility, from a reason to give up, into a reason to continue. 

Who cares if you don't finish, who cares if you are embarrassed by your finishing time, who cares who beat you? You can feel a great sense of accomplishment in simply taking the next step and battling on regardless.

When I recently lost 25kg in 25 weeks, transforming myself from unhealthy and obsese to a lean competitive athlete, I dealt with my hunger pangs and the temptations of the larder by taking pride in my ability tp feel the desire, yet be sufficiently Stupidly Determined to resist it. The devil on my shoulder that whispers "what does it really matter", "you can always lose more weight tomorrow", "you'll only get more and more hungry and give in eventually" is powerless against Stupidly Determined.

When I subsequently competed in my first triathlon in 15 years, I found myself with 2km to the finish line, deep into the third and final leg of the race running with sore hips and the initial twinge of cramps, with no one visible ahead of me to catch and no chance to win my age group. I could hear the voice again: "who cares if you walk a bit", "you don't have anything left to prove", "you're not really an athlete", "you're going to be embarrassed by the time anyway, you might as well make it awful, at least you can get some sympathy". But Stupidly Determined kept me going and now I can look back with real pride at having posted the fastest run split in my age group.

It's funny that two words on the T-Shirt of an ordinary person, facing the ordinary, quotidian labour of overcoming ordinary human weakness can be such a profound inspiration, when the exploits of the great and mighty are so impotent to affect us. Heroes can dream of achieving immortality in legend through acts of noble sacrifice. For the rest of us, there is Stupidly Determined.


[^1]: See [Wikipedia: Samurai/Philosophy](https://en.wikipedia.org/wiki/Samurai#Philosophy) and [Wikipedia: Bushido/Tenets](https://en.wikipedia.org/wiki/Bushido#Tenets) for more on courage and honour as it relates to the Samurai

[^2]: An interesting recent study about cognitive reappraisal can be found [here](https://digest.bps.org.uk/2018/06/29/a-mental-technique-calledcognitive-reappraisal-makes-long-distance-running-feel-easier/)

[^3]: See this TED talk by Angela Duckworth on the value of [Grit](https://www.ted.com/talks/angela_lee_duckworth_grit_the_power_of_passion_and_perseverance).

[^4]: It is well worth reading this book even if you have no interest in tennis. It contains a great deal of wisdom that applies widely to all kinds of performance. You can find it on [Amazon](https://www.amazon.com/Inner-Game-Tennis-Classic-Performance/dp/0679778314).
